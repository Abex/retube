#ReTube.user.js
ReTube.user.js is a user script that (will) completely replace the standard youtube front end with its own. Currently it works by hijacking `http*://*youtube.com/watch*` and redirecting to `http*://*youtube.com/atch*`, which proceeds to 404, creating a (almost) blank canvas for ReTube to use. 

###Current Features
- Works with standard YouTube URLs
- Can be turned off temporarily
- Configurable
- Doesn't require flash
- Works with AdBlockers (YouTube normally will error your video if you have one)

###Planned Features (in order of importance)
- Custom chromed player
- Rating support
- Pretty UI
- 1080p+ support
- Subscription support
- (Optional) comment support


#####Note: This is under HEAVY DEVELPOMENT and probably doesn't do what you want it too.

##You can [Install Here](https://bitbucket.org/Abex/retube/raw/master/src/ReTube-greasemonkey.user.js).
###For Chrome/Chromium Users!
For ReTube to work under the Chrome family you must have [Tapermonkey](http://tampermonkey.net/) installed.
Tapermonkey must also have access to `file:\\` URIs. To do this goto [chrome://extensions/](chrome://extensions/), select Tapermonkey, and check the `Allow access to file URLs` checkbox.
[Nightly install](https://bitbucket.org/Abex/retube/raw/nighltly/src/ReTube-greasemonkey.user.js)