// ==UserScript==
// @name        ReTube
// @namespace   Abex
// @description Under Development
// @include     http*://*youtube.com/watch*
// @include     http*://*youtube.com/atch*
// @resource    jquery.js common/jquery-2.0.3.min.js
// @resource    index.phtm common/index.phtm
// @resource    watch.phtm common/watch.phtm
// @resource    home.phtm common/home.phtm
// @resource    settings.phtm common/settings.phtm
// @resource    default.css common/default.css
// @resource    safe.js common/safe.js
// @version     0.0.4
// @grant       GM_getResourceText
// @run-at      document-start
// ==/UserScript==
function $_GET( name ){
  name = name.replace(/[\[]/,"\\\[").replace(/[\]]/,"\\\]");
  var regexS = "[\\?&]"+name+"=([^&#]*)";
  var regex = new RegExp( regexS );
  var results = regex.exec( window.location.href );
  if( results == null )
    return "";
  else
    return results[1];
}
var cHref=window.location.href;
if(cHref.indexOf("watch")>-1) // on watch*
{
  if($_GET('nofix')=="" && $_GET('nofeather')=="")
  {
    var acHref = cHref.split("");
    delete acHref[cHref.indexOf("watch")];
    location.replace(acHref.join(""));
  }
}
else // on atch* -- we have the power ----------------------------------------------------------------------------------------------
{ // func defs are not at the top so intereption of js/net/dom rendering/anything that isnt use happens first
function waitForHead() // head will load first - check that
{
  if(document.head && document.head.innerHTML)
  {
    document.head.innerHTML="<title></title><link href='//netdna.bootstrapcdn.com/font-awesome/4.0.3/css/font-awesome.css' rel='stylesheet'><link rel='shortcut icon' id='favicon'>";
  }
  else
  {
    setTimeout(waitForHead,1);
  }
}
waitForHead(); // run preliminary head check
function waitForBody() // head will load 2nd - check that
{
  if(document.body && document.body.innerHTML)
  {
    unsafeWindow.document.body.innerHTML="";
  }
  else
  {
    setTimeout(waitForBody,1);
  }
}
function conf(s)
{
  var rVal;
  if (typeof localStorage["fixyt."+s] != "undefined")
  {
    rVal=localStorage["fixyt."+s];
  }
  else
  {
    rVal={"debug":"true"}[s] || "false"; // defaults for conf(string)
  }
  return rVal;
}
function bConf(s)
{
  var rVal=conf(s);
  if(rVal=="TRUE" || rVal=="true" || rVal=="1" || rVal==1 || rVal==true)
  {
    return true;
  }
  return false;
}

waitForBody(); // run preliminary body check
// We have a blank pqage
var resources = ['jquery.js','index.phtm','safe.js','watch.phtm','home.phtm','settings.phtm','default.css'];
unsafeWindow.assets={};
for(iResource in resources)
{
  if(bConf('debug'))
  {
    console.log("Loading asset "+resources[iResource]);
  }
  unsafeWindow.assets[resources[iResource]]=GM_getResourceText(resources[iResource]);
}
unsafeWindow.assets['version']=GM_info.script.version;
if(bConf('debug'))
{
  console.log("Injecting jquery");
}
var Ejquery = document.createElement("script"); // inject us
Ejquery.innerHTML=unsafeWindow.assets['jquery.js'];
Ejquery.type="text/javascript";
document.head.appendChild(Ejquery);
if(bConf('debug'))
{
  console.log("Injecting safe.js");
}
var Esafe = document.createElement("script");
Esafe.innerHTML="\n"+unsafeWindow.assets['safe.js'];
Esafe.type="text/javascript";
document.head.appendChild(Esafe);
}